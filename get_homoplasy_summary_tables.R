
# AnnoTree Manuscript Scripts
# Copyright (C) 2019  Kerrin Mendler (mendlerke@gmail.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


library(KEGGREST)
library(PFAM.db)
library(phangorn)
library(phytools)

tree <- read.tree("gtdb_r83_bac120.tree")


#################################################
## Functions
#################################################
getCI <- function(tree, tip_states) {
  tip_states.phyDat <- as.phyDat(setNames(as.character(tip_states),
                                          names(tip_states)), 
                                 type = "USER",
                                 levels = c("0", "1"))
  return(CI(tree, tip_states.phyDat, sitewise = TRUE))
}


#################################################
## KEGG orthologs
#################################################
kegg_tip_states <- read.delim("gtdb_r83_ko_table.tsv", row.names = 1)
kegg_tip_states[kegg_tip_states > 1] <- 1
kegg_tip_states <- kegg_tip_states[tree$tip.label, ]

# Get family size
kegg_family_size <- colSums(kegg_tip_states)

# Calculate consistency index (CI) for each KO term
kegg_CIs <- apply(kegg_tip_states, 2, getCI, tree = tree)

# Retrieve KEGG descriptions
x <- keggList("ko")
ko.kegg.tmp <- sapply(names(kegg_CIs), 
                      function(kegg_id){
                        individual_kegg_ids <- strsplit(kegg_id, "\\.")
                        all_terms <- sapply(individual_kegg_ids, 
                                            function(id){
                                              paste("ko", id, sep = ":")
                                            })
                      }, 
                      USE.NAMES = FALSE)
kegg_descriptions <- sapply(ko.kegg.tmp, function(entry) {
  all_entry_descriptions <- sapply(entry, function(y) {
    description <- tryCatch({
      return(x[[y]])
    }, error = function(e) {
      return("NA")
    })
  }, USE.NAMES = FALSE)
  return(paste(all_entry_descriptions, collapse = " & "))
})
compound_kegg_ids_for_display <- sapply(names(kegg_CIs), 
                                        gsub, pattern = ".", 
                                        replacement = "&", fixed = TRUE,
                                        USE.NAMES = FALSE)

# Tie it all together & export
kegg_summary_table <- data.frame(kegg = compound_kegg_ids_for_display,
                                 description = kegg_descriptions,
                                 CI = kegg_CIs,
                                 normCI = log(kegg_CIs)/log(kegg_family_size),
                                 family_size = kegg_family_size)
kegg_summary_table <- kegg_summary_table[order(kegg_summary_table$kegg),]
write.table(kegg_summary_table, "kegg_homoplasy_summary.txt", 
            sep = "\t", quote = FALSE, row.names = FALSE)

# Clean up RAM
rm(kegg_tip_states, kegg_family_size, kegg_CIs, x, ko.kegg.tmp, 
   kegg_descriptions, compound_kegg_ids_for_display)


#################################################
## Pfams
#################################################
pfam_tip_states <- read.delim("gtdb_r83_pfam_table.tsv", row.names = 1)
pfam_tip_states[pfam_tip_states > 1] <- 1
pfam_tip_states <- pfam_tip_states[tree$tip.label, ]

# Get family size
pfam_family_size <- colSums(pfam_tip_states)

# Calculate consistency index (CI) for each Pfam
pfam_CIs <- apply(pfam_tip_states, 2, getCI, tree = tree)

# Retrieve Pfam descriptions
acc2de <- as.list(PFAMDE)
pfam_descriptions <- sapply(colnames(pfam_tip_states), 
                            function(pfam_id){
                              pfam_acc <- strsplit(pfam_id, "\\.")[[1]][1]
                              pfam_description <- acc2de[[pfam_acc]]
                              if(is.null(pfam_description)) {
                                return("NA")
                              }
                              return(pfam_description)
                            })

# Tie it all together & export
pfam_summary_table <- data.frame(pfam = colnames(pfam_tip_states),
                                 description = pfam_descriptions,
                                 CI = pfam_CIs,
                                 normCI = log(pfam_CIs)/log(pfam_family_size),
                                 family_size = pfam_family_size)
pfam_summary_table <- pfam_summary_table[order(pfam_summary_table$pfam),]
write.table(pfam_summary_table, "pfam_homoplasy_summary.txt", 
            sep = "\t", quote = FALSE, row.names = FALSE)
