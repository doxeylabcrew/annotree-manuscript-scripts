# AnnoTree Manuscript Scripts

## Data Sources
Scripts in this repository manipulate a phylogenetic tree, a taxonomy file, a Pfam counts table, and a KEGG ortholog counts table from GTDB Release 02-RS83. They are hosted by the GTDB team [here](https://data.ace.uq.edu.au/public/misc_downloads/annotree/r83/).

## Scripts
File    | Description | Required Files
------- | ----------- | --------------
`metric_benchmarking.R` | Test the processing speed of functions that calculate homoplasy and phylogenetic signal metrics on a small set of annotation profiles | `gtdb_r83_bac120.tree`,  `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`
`contamination_effects_on_normCI.R` | Calculate and then recalculate the consistency index (CI) with putative contaminants removed from the annotation profile for both Pfams and KOs. A rank vs. rank plot is generated and Kendall's *W* for concordance is calculated for each recalculation | `gtdb_r83_bac120.tree`,  `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`
`get_homoplasy_summary_tables.R` | Produce intermediate summary tables (`pfam_homoplasy_summary.txt` and `kegg_homoplasy_summary.txt`) with annotation ID, description, consistency index (CI), normalized CI, and family size | `gtdb_r83_bac120.tree`,  `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`
`kegg_homoplasy_plot.R` | Produce box plots of KEGG PATH and KEGG BRITE categories coloured by higher-level functional category with custom labels for a small list of categories | `kegg_homoplasy_summary.txt` from `get_homoplasy_summary_tables.R`
`lineage_specific_annotations.R` | Classify Pfam and KEGG annotations as lineage-specific at given catchment and saturation thresholds (See Mendler et al. (2018) for method); perform sensitivity test of lineage-specific counts to saturation and catchment; produce bar plots and data tables of lineage-specific trait frequency for taxonomic levels > species | `gtdb_r83_bac120.tree`, `bac_taxonomy_r83.tsv`, `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`
`lineage_specific_annotations.f1score.R` | Classify Pfam and KEGG annotations as lineage-specific through use of the F1 score/F measure (See Mendler et al. (2018) for method); produce bar plots and data tables of lineage-specific trait frequency for all taxonomic levels | `gtdb_r83_bac120.tree`, `bac_taxonomy_r83.tsv`, `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`
`homoplasy_taxon_enrichment.R` | Test for enrichment of the most homoplasic annotations in taxa at each level and produce a results table | `bac_taxonomy_r83.tsv`, `gtdb_r83_bac_ko_table.tsv`, `gtdb_r83_bac_pfam_table.tsv`, and `kegg_homoplasy_summary.txt` and `pfam_homoplasy_summary.txt` from `get_homoplasy_summary_tables.R`

## Citing this work
If you use any of the scripts in this repository or the AnnoTree tool, please consider citing:  
AnnoTree: visualization and exploration of a functionally annotated microbial tree of life: Mendler, K., Chen, H., Parks, D. H., Hug, L. A. and Doxey, A. C.  
**bioRxiv** (2018) doi: https://doi.org/10.1101/463455

If you use any of the data hosted by the GTDB team, please cite:  
A standardized bacterial taxonomy based on genome phylogeny substantially revises the tree of life: Donovan, H. P., Chuvochina, M., Waite, D. W., Rinke, C., Skarshewski, A., Chaumeil, P. and Hugenholtz P.  
**Nature Biotechnology** (2018) doi: https://doi.org/10.1038/nbt.4229

## License
The code in this repository is released under the [GNU GPLv3 license](https://bitbucket.org/kmendler/annotree-manuscript-scripts/src/master/LICENSE).
